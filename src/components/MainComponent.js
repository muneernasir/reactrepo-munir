import React, { Component } from 'react';
import Menu from './MenuComponent';
import Dishdetails from './DishdetailComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import { DISHES } from '../shared/dishes'
import Home from './HomeCompoment';
import contact from './contactComponent';
import About from './AboutComponent';
import { Switch, Route, Redirect } from 'react-router-dom';
import { COMMENTS } from '../shared/coments';
import { LEADERS } from '../shared/leaders';
import { PROMOTIONS } from '../shared/promotions';

class Main extends Component {
constructor(props) {
  super(props);

  this.state = {
    dishes: DISHES,
    comments: COMMENTS,
    promotions: PROMOTIONS,
    leaders : LEADERS,
    selectedDish: null
  };
}


onDishSelect(dishId) {
    this.setState({ selectedDish: dishId});
}


  render() {
    const HomePage = () => {
      return (
        <Home dish={this.state.dishes.filter((dish) => dish.featured)[0]}   
          promotion={this.state.promotions.filter((promo) => promo.featured)[0]}
          leader={this.state.leaders.filter((leader) => leader.featured)[0]}
          />
      )
    }

    return (
      <div>      
        <Header/>  
        <Switch>
          <Route path="/home"  component={HomePage} />
          
          <Route exact path="/menu" component={() => <Menu dishes = {this.state.dishes} /> } />
          <Route exact path="/contactus" component={contact}/>
          <Route path="/aboutus" component={About} />
          <Redirect to="/home>" />
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default Main;
